/*+
    VIDE -- Void IDentification and Examination -- ./c_tools/mock/loaders/flash_loader.cpp
    Copyright (C) 2010-2014 Guilhem Lavaux
    Copyright (C) 2011-2014 P. M. Sutter

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
+*/



#include <cassert>
#include <string>
#include <CosmoTool/loadFlash.hpp>
#include <CosmoTool/fortran.hpp>
#include "../../../external/FOFReaderLib/FOFReaderLib/FOFReaderLib.h"
#include "simulation_loader.hpp"
#include "generateMock_conf.h"

using namespace std;
using namespace CosmoTool;

class FofLoader: public SimulationLoader
{
private:
  int load_flags;
  bool onefile;
  int _num_files;
  SimuData *fof_header;
  string snapshot_name;
  SimulationPreprocessor *preproc;
public:
  FofLoader(const string& basename, SimuData *header, int flags, bool singleFile, int _num, SimulationPreprocessor *p)
    : snapshot_name(basename), load_flags(flags), onefile(singleFile), _num_files(_num), fof_header(header), preproc(p)
  {
  }
  
  ~FofLoader()
  {
    delete fof_header;
  }
  
  SimuData *getHeader() {
    return fof_header;
  }
  
  int num_files() {
    return _num_files;
  }
  
  SimuData *loadFile(int id) {
    SimuData *data;
    
    if (onefile && id > 0)
      return 0;
    if (id >= _num_files)
      return 0;
    
    cout << "Loading " << snapshot_name <<  endl;

    if (onefile) {        
        
        data = new SimuData;
        
        data->time    = fof_header->time;
        data->TotalNumPart = fof_header->TotalNumPart;
        data->BoxSize = fof_header->BoxSize;
        data->Omega_M = fof_header->Omega_M;
        data->Omega_Lambda = fof_header->Omega_Lambda;
        data->Hubble = fof_header->Hubble;
        
        FOFMultiCube *cubeFile = new FOFMultiCube(snapshot_name, FOFParticles::DONT_READ_PARTICLES);
        
        data->TotalNumPart = data->NumPart = cubeFile->npart();//npart;
        
        // particle data
        if (load_flags& NEED_POSITION) {
          for (int i = 0; i < 3; i++) {
            data->Pos[i] = new float[data->NumPart];
            if (data->Pos[i] == 0) {
              delete data;
              return 0;
            }
          } 
        }
        
        if (load_flags &NEED_VELOCITY) {
            for (int i = 0; i < 3; i++) {
              data->Vel[i] = new float[data->NumPart];          
              if (data->Vel[i] == 0) {              
                delete data;
                return 0;
              }
            } 
        }
        
        if (load_flags & NEED_GADGET_ID) {
            data->Id = new long[data->NumPart];
            if (data->Id == 0) {
              delete data;
              return 0;
            }
        }

        int offset = 0;
        int pos = 0;
        for (int c = 0; c < cubeFile->nCubes(); c++) {
            cubeFile->cubes(c)->readParticles(FOFParticles::READ_ALL);
            for (int n = 0; n < cubeFile->cubes(c)->npart(); n++) { // data->NumPart
              data->Pos[0][pos] = cubeFile->cubes(c)->posX(n) * data->BoxSize;
              data->Pos[1][pos] = cubeFile->cubes(c)->posY(n) * data->BoxSize;
              data->Pos[2][pos] = cubeFile->cubes(c)->posZ(n) * data->BoxSize;
              data->Vel[0][pos] = cubeFile->cubes(c)->velX(n);
              data->Vel[1][pos] = cubeFile->cubes(c)->velY(n);
              data->Vel[2][pos] = cubeFile->cubes(c)->velZ(n);
              data->Id[pos] = cubeFile->cubes(c)->id(n);
              pos++;
            }
            cubeFile->cubes(c)->releaseParticles();
        }
    
    }
      
    else {
      data = 0;//loadFlashMulti(snapshot_name.c_str(), id, load_flags);  
    }
      
    cout << "Particle 0 READ : (" << data->Pos[0][0] << "," << data->Pos[1][0] << "," << data->Pos[2][0] << ")" << endl;
    
    if (data->Id != 0)
      {
	long *uniqueID = new long[data->NumPart];
       for (long i = 0; i < data->NumPart; i++)
	  {
	    uniqueID[i] = data->Id[i];
	  }       
	data->new_attribute("uniqueID", uniqueID, delete_adaptor<long>);
      }

    applyTransformations(data);
    basicPreprocessing(data, preproc);
    
    return data;
  }
};


SimulationLoader *fofLoader(const std::string& snapshot, int flags, struct generateMock_info *args_info, SimulationPreprocessor *p)
{
  bool singleFile;
  int num_files;
  SimuData *header;
  
  singleFile = true;
  num_files = 1;
  
  FOFMultiCube *cubeFile = new FOFMultiCube(snapshot, FOFParticles::DONT_READ_PARTICLES);
    
  header = new SimuData;  
  header->time    = 1; //1/(1+redshift);
  header->TotalNumPart = header->NumPart = cubeFile->npart();//npart;
  header->BoxSize = args_info->fof_boxlen_arg; // Mpc
  header->Omega_M = args_info->fof_omegaM_arg; 
  header->Omega_Lambda = 1.0 - args_info->fof_omegaM_arg; 
  header->Hubble = args_info->fof_hubble_arg; 
  
  if(args_info->rangeX_min_arg < cubeFile->minX() * header->BoxSize) {args_info->rangeX_min_arg = cubeFile->minX() * header->BoxSize;}
  if(args_info->rangeX_max_arg > cubeFile->maxX() * header->BoxSize) {args_info->rangeX_max_arg = cubeFile->maxX() * header->BoxSize;}
  if(args_info->rangeY_min_arg < cubeFile->minY() * header->BoxSize) {args_info->rangeY_min_arg = cubeFile->minY() * header->BoxSize;}
  if(args_info->rangeY_max_arg > cubeFile->maxY() * header->BoxSize) {args_info->rangeY_max_arg = cubeFile->maxY() * header->BoxSize;}
  if(args_info->rangeZ_min_arg < cubeFile->minZ() * header->BoxSize) {args_info->rangeZ_min_arg = cubeFile->minZ() * header->BoxSize;}
  if(args_info->rangeZ_max_arg > cubeFile->maxZ() * header->BoxSize) {args_info->rangeZ_max_arg = cubeFile->maxZ() * header->BoxSize;}
  
  delete cubeFile;
    
  return new FofLoader(snapshot, header, flags, singleFile, num_files, p);
}
