/* 
 * File:   FOFReaderLib.h
 * Author: admin
 *
 * Created on 8 octobre 2013, 12:26
 */

#ifndef FOFREADERLIB_H
#define	FOFREADERLIB_H

#include "FOFFiles/FOFFile.h"
#include "FOFFiles/FOFCube.h"
#include "FOFFiles/FOFMultiCube.h"
#include "FOFFiles/FOFStrct.h"
#include "FOFFiles/FOFMasst.h"
#include "FOFFiles/FOFHalo.h"
#include "FOFFiles/FOFCubeGrav.h"
#include "DEUSSimulation/DEUSHalos.h"
#include "DEUSSimulation/DEUSCubes.h"
#include "DEUSSimulation/DEUSGrav.h"
#include "DEUSSimulation/DEUSArea.h"
#include "DEUSSimulation/DEUSSimulationSingleton.h"

#endif	/* FOFREADERLIB_H */

